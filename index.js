module.exports = async (app, config) => {
    const express = require('express')

    app.use(config.getRouteName('/static'), express.static(config.getPath('static')))

    app.loadApiFunctions({
        path: config.getPath('api'),
        scope: ({ req }) => {
            let dbName = config.db_name
            return {
                moduleId: config.id,
                dbName
            }
        }
    })

    app.get(
        config.getRouteName('app.js'),
        app.webpackMiddleware({
            entry: config.getPath('app.js'),
            output: config.getPath('tmp/app.js')
        })
    )

    app.get(
        config.getRouteName('/'),
        app.builder.transformFileRoute({
            cwd: config.getPath(),
            source: 'app.pug',
            mode: 'pug',
            transform: [app.cacheCDNScripts],
            context: {
                cwd: config.getRouteName(),
                head: {
                    title: config.title
                }
            }
        })
    )
}