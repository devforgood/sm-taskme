import {default as stylesMixin, template as stylesTpl} from '../mixins/styles'

export default {
    mixins: [stylesMixin],
    name: 'addBoard',
    props: [],
    template: stylesTpl(`
        <div class="addBoard" ref="root" >
            <h2>Add board</h2>
            <div class="ab_inputs">
                <div class="abi_group">
                    <label>Name</label>
                    <input v-model="form.name"/>
                </div>
                <div class="abi_group">
                    <button @click="save" class="btn">Save</button>
                </div>
            </div>
        </div>
    </div>
    `),
    data() {
        var self = this
        return {
            styles: `
            .addBoard{
            
            }
            .ab_inputs{

            }
            .abi_group{
                margin-top:30px;
            }
            
            @media only screen and (max-width: 639px) {
                
            }`,
            form:{
                name:''
            }
        }
    },
    computed: {
       
    },
    methods: {
       async save(){
           await api.funql({
               name:'taskmeBoardAdd',args:[Object.assign({},this.form)]
           })
           this.$router.push({
               name:'dashboard'
           })
       }
    },
    mounted() {
        
    }
}