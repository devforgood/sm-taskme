module.exports = app => {
    var debug = require('debug')(`app:api:taskmeBoardAdd ${`${Date.now()}`.white}`)
    return async function taskmeBoardAdd(form) {
        let creation_date = require('moment-timezone')().tz('Europe/Paris')._d.getTime()
        return await app.dbExecute(
            `
INSERT INTO boards
(name,creation_date)
VALUES(?,?)
    `,
            [
                form.name,
                creation_date
            ],
            {
                dbName: this.dbName
            }
        )
    }
}