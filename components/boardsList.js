import { default as stylesMixin, template as stylesTpl } from '../mixins/styles'

Vue.component('boards-list', {
    mixins: [stylesMixin],
    props: [],
    template:
        stylesTpl(`
    <div class="boards_list" ref="root">
        <button class="btn" @click="refresh">Refresh</button> 
        <div class="bl_list">
            <div class="bll_item"
            v-for="(item, index) in items" :key="item.id"    
            >
                <div class="blli_content">
                    <div class="blli_title" v-html="item.name"></div>
                </div>
            </div>
        </div>
    </div>
</div>
`),
    destroyed() { },
    methods: {
        async refresh() {
            this.items = await api.funql({
                name: "selectQuery",
                args: [{
                    query: `* FROM boards`, queryArgs: []
                }]
            })
        }
    },
    computed: {},
    async mounted() {
        await this.refresh()
    },
    data() {
        return {
            items: [],
            styles: `
                .boards_list{
                  
                }
                .bl_list{
                    display: grid;
                    grid-template-columns: 200px;
                }
                .bll_item{
                    flex-direction: column;
                    display: block;
                    min-height: 50px;
                    min-width: 150px;
                    background-color: darkkhaki;
                    margin: 10px 10px 10px 0px;
                }
                .blli_content{
                    display:flex;
                    justify-content:center;
                    align-items:center;
                    color:white;
                    height: 100%;
                }
                .blli_title{
                    font-size:25px;
                }
               
`
        }
    }
})